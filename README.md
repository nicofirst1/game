# map stuff
## Links 
- [map](https://www.eternalcoding.com/?p=263)
- [textures](https://opengameart.org/textures)
- [skyboxes](http://www.custommapmakers.org/skyboxes.php)
 
## skyboxes
- [image distribution](http://iwearshorts.com/Mike/uploads/2015/04/map-300x225.jpg)

### Notation conversion
babylon work with the notation [px,py,pz,nx,ny,nz] shown in - [this image](http://iwearshorts.com/Mike/uploads/2015/04/map-300x225.jpg)
, if you have another notation one possible conversion is this:

- up->py
- ft->pz
- rt->nx
- lf->px
- dn->ny
- bk->nz